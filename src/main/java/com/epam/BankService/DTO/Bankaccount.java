package com.epam.BankService.DTO;

public class Bankaccount {
    private String bankaccount;
    private int summ;
    private boolean status;

    public Bankaccount(String bankaccount, int summ,boolean status) {
        this.bankaccount = bankaccount;
        this.summ = summ;
        this.status=status;
    }

    public String getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(String bankaccount) {
        this.bankaccount = bankaccount;
    }

    public int getSumm() {
        return summ;
    }

    public void setSumm(int summ) {
        this.summ = summ;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Bankaccount{" +
                "bankaccount='" + bankaccount + '\'' +
                ", summ=" + summ +
                ", status=" + status +
                '}';
    }
}
