package com.epam.BankService.Repository;

import com.epam.BankService.DTO.Bankaccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ClientRepositoryImpl implements ClientRepository {
    @Autowired
    BankaccountRepository bankaccountRepository;

    public BankaccountRepository getBankaccountRepository() {
        return bankaccountRepository;
    }

    @Override
    public void pay(String bankaccount, int summ) {
        for (Bankaccount bankaccount1: bankaccountRepository.getBankaccounts()
             ) {
            if(bankaccount1.getBankaccount().equals(bankaccount))
                bankaccount1.setSumm(bankaccount1.getSumm() - summ);
        }
    }

    @Override
    public void lock(String bankaccount) {
        for (Bankaccount bankaccount1: bankaccountRepository.getBankaccounts()
                ) {
            if(bankaccount1.getBankaccount().equals(bankaccount))
                bankaccount1.setStatus(false);
        }

    }

    @Override
    public void replenish(String bankaccount, int summ) {
        for (Bankaccount bankaccount1 : bankaccountRepository.getBankaccounts()
                ) {
            if (bankaccount1.getBankaccount().equals(bankaccount))
                bankaccount1.setSumm(bankaccount1.getSumm() + summ);

        }
    }
}
