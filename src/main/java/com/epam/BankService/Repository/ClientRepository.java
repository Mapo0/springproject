package com.epam.BankService.Repository;

public interface ClientRepository {
    public void pay(String bankaccount,int summ);
    public void lock(String bankaccount);
    public void replenish(String bankaccount, int summ);
}
