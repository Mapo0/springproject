package com.epam.BankService.Repository;

import com.epam.BankService.DTO.Bankaccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AdminRepositoryImpl implements AdminRepository {
    @Autowired
    BankaccountRepository bankaccountRepository;
    @Override
    public void unlock(String bankaccount) {
        for (Bankaccount bankaccount1: bankaccountRepository.getBankaccounts()
                ) {
            if(bankaccount1.getBankaccount().equals(bankaccount))
                bankaccount1.setStatus(true);
        }

    }

    }

