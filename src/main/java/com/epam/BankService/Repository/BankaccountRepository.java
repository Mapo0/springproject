package com.epam.BankService.Repository;

import com.epam.BankService.DTO.Bankaccount;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
@Repository
public class BankaccountRepository {
    private List<Bankaccount> bankaccounts = Arrays.asList(
            new Bankaccount("12q",259,true),
            new Bankaccount("34w",23243,true),
            new Bankaccount("56e",99999999,false));

    public List<Bankaccount> getBankaccounts() {
        return bankaccounts;
    }
}
