package com.epam.BankService;

import com.epam.BankService.Repository.AdminRepository;
import com.epam.BankService.Repository.AdminRepositoryImpl;
import com.epam.BankService.Repository.ClientRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BankServiceApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(BankServiceApplication.class, args);
		ClientRepositoryImpl clientRepository = (ClientRepositoryImpl) context.getBean("clientRepositoryImpl");
		AdminRepositoryImpl adminRepository = (AdminRepositoryImpl) context.getBean("adminRepositoryImpl");
		clientRepository.pay("12q",100);
		clientRepository.lock("12q");
		clientRepository.replenish("34w",1000);
		adminRepository.unlock("56e");
		System.out.print(clientRepository.getBankaccountRepository().getBankaccounts());
	}

}
